from pymongo import MongoClient

from os import listdir
mongoclient = MongoClient('mongodb://localhost:27017')
import asyncio

import json
import os

import discord
from discord.ext import commands

bot = commands.Bot(command_prefix=";", help_command=None)

helpText = {}
with open('./helpText') as helpFile:
    helpString = helpFile.read()
helpText['general'] = helpString.split("<general>\n")[1].split("\n</general>")[0]
helpText['random'] = helpString.split("<random>\n")[1].split("\n</random>")[0]
helpText['numgen'] = helpString.split("<numgen>\n")[1].split("\n</numgen>")[0]
helpText['misc'] = helpString.split("<misc>\n")[1].split("\n</misc>")[0]
helpText['support'] = helpString.split("<support>\n")[1].split("\n</support>")[0]


def generate_config_reload():
    global config
    with open('config.json') as configFile:
        config = json.load(configFile)


@bot.event  # Sets status on start and prints that it's logged in
async def on_ready():
    print('\nLogged in as {0.user}'.format(bot))
    await bot.change_presence(
        activity=discord.Game(
            name=";help || New Help Menu & Commands"
        )
    )


@bot.event  # When the bot joins a guild, it adds the default prefix and server ID to a database table
async def on_guild_join(guild):  # Logs when the bot joins a guild (does not log ID, so don't worry)
    # Defining variables
    mndb = mongoclient['the-random-bot']
    servercol = mndb['servers']
    serverID = guild.id
    serverName = guild.name
    serverDict = { 'serverID': serverID, "serverName": serverName, "prefix": ";" }
    dbWrite = servercol.insert_one(serverDict)
    channel = bot.get_channel(811010228945682432)
    joinDesc = "Server ID: " + str(serverID) + "\nServer Name: " + str(serverName) + "\nDatabase Result: " + str(dbWrite)
    joinServerEmbed = discord.Embed(title="Added to a new server!", description=joinDesc)
    await channel.send(embed=joinServerEmbed)


@bot.command()
async def generate(ctx):
    deprecateWarning = discord.Embed(title="This command has been deprecated.",
                                     description="Please use `;help` to see the new commands, as this command set has been distributed to cogs.",
                                     color=0xC73333)
    await ctx.channel.send(embed=deprecateWarning)


# The Token initialization and Checking if config.json exists
if __name__ == '__main__':
    if not os.path.exists('config.json'):
        keysGen = str(input("ERROR: DID NOT FIND A CONFIG.JSON FILE\nWould you like to make a config.json file? (Y/N) ")).lower()
        if keysGen == 'y':
            config = {} # Create the empty dictionary for configuration
            print("----Note: We do not receive your API keys, these are stored in a file on your computer.----")
            print("The config.json file created by this should be considered sensitive. DO NOT share it with anyone.")
            config['ytApiKey'] = str(input("Please input your YouTube API Developer Key: "))
            config['lastFmKey'] = str(input("Please input your last.fm API key: "))
            config['lastFmUA'] = str(input("Please input the User Agent that should be used when requesting from the last.fm API: "))
            config['discordToken'] = str(input("Please input your Discord bot token: "))
            config['bannedWords'] = str(input("Please input a list of words you don't want the bot to repeat, seperated by a comma.")).split(',')
            config['developers'] = {} # Create the empty developers dictionary
            print("The next section will allow you to choose who to give access to developer commands. Developers will be able to change the bot's status, see bot statistics, and add other developers. Make sure you trust anyone you add.")
            print("Who will your first developer be? You'll be able to add other ones through the bot later.")
            config['developers'][str(input("User ID of the first dev: "))] = str(input("Name of the first dev: "))
            print("Setup complete, running the bot...")
            with open('config.json', 'w+') as configFile:
                json.dump(config, configFile, indent=4)
            generate_config_reload()
        elif keysGen == 'n':
            print("Exiting... Please remember to make a config.json file in order for the bot to be fully functional.")
            quit()
        print("Creating... Done! Your self-hosted bot is now live!")
        bot.run(config['discordToken'])
    elif os.path.exists('config.json'):
        with open('config.json') as configFile:
            config = json.load(configFile)
        generate_config_reload()
        for cog in listdir('./cogs'):
            if cog.endswith('.py'):
                bot.load_extension(f'cogs.{cog[:-3]}')
        bot.run(config['discordToken'])
