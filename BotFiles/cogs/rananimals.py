from discord.ext import commands
import discord
import json
import requests


class Animals(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def dog(self, ctx):
        response = json.loads(requests.get("https://dog.ceo/api/breeds/image/random").text)
        dogEmbed = discord.Embed(title="I fetched this dog for you 🦴",
                                 color=0xB87DDF)
        dogEmbed.set_image(url=f"{response['message']}")
        dogEmbed.set_footer(text="Generated with the Dog CEO API")
        await ctx.channel.send(embed=dogEmbed)

    @commands.command()
    async def fox(self, ctx):
        response = json.loads(requests.get("https://randomfox.ca/floof/").text)
        foxEmbed = discord.Embed(title="I found this image of a fox!",
                                 color=0xB87DDF)
        foxEmbed.set_image(url=f"{response['image']}")
        foxEmbed.set_footer(text="Generated with the randomfox.ca API")
        await ctx.channel.send(embed=foxEmbed)

    @commands.command()
    async def cat(self, ctx):
        response = json.loads(requests.get("https://some-random-api.ml/img/cat").text)
        catEmbed = discord.Embed(title="I found this image of a cat!",
                                 color=0xB87DDF)
        catEmbed.set_image(url=f"{response['link']}")
        await ctx.channel.send(embed=catEmbed)

    @commands.command()
    async def panda(self, ctx):
        response = json.loads(requests.get("https://some-random-api.ml/img/panda").text)
        pandaEmbed = discord.Embed(title="I found this image of a panda!",
                                   color=0xB87DDF)
        pandaEmbed.set_image(url=f"{response['link']}")
        await ctx.channel.send(embed=pandaEmbed)

    @commands.command()
    async def redpanda(self, ctx):
        response = json.loads(requests.get("https://some-random-api.ml/img/red_panda").text)
        redpandaEmbed = discord.Embed(title="I found this image of a red panda!",
                                      color=0xB87DDF)
        redpandaEmbed.set_image(url=f"{response['link']}")
        await ctx.channel.send(embed=redpandaEmbed)

    @commands.command()
    async def bird(self, ctx):
        response = json.loads(requests.get("https://some-random-api.ml/img/birb").text)
        birbEmbed = discord.Embed(title="I found this image of a birb!",
                                  color=0xB87DDF)
        birbEmbed.set_image(url=f"{response['link']}")
        await ctx.channel.send(embed=birbEmbed)

    @commands.command()
    async def koala(self, ctx):
        response = json.loads(requests.get("https://some-random-api.ml/img/koala").text)
        koalaEmbed = discord.Embed(title="I found this image of a koala!",
                                   color=0xB87DDF)
        koalaEmbed.set_image(url=f"{response['link']}")
        await ctx.channel.send(embed=koalaEmbed)


def setup(bot):
    bot.add_cog(Animals(bot))
