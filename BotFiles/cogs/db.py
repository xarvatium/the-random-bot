from discord.ext import commands
import discord
from pymongo import MongoClient
import json

mongoclient = MongoClient('mongodb://localhost:27017')

with open('./config.json') as configFile:
    config = json.load(configFile)


class DB(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()  # Shows the database
    async def dbshow(self, ctx):
        bot = self.bot
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)  # Error Embed
        if str(ctx.message.author.id) in config['developers']:  # Checks if user is a developer
            servers = list(bot.guilds)  # Gets a list of servers
            mndb = mongoclient['the-random-bot']  # Connects to the database
            servercol = mndb['servers']  # Gets the table
            dbquery = servercol.find()  # Gets the column to iterate through
            empty = ""
            entry = 1
            for data in dbquery:  # Loops through the query
                empty += "```md\n# Entry " + str(entry) + ":\n" + str(data) + "```\n"  # Appends to the empty string
                entry += 1
            await ctx.channel.send(empty)  # Sends the result
        else:
            await ctx.channel.send(embed=notDevEmbed)

    @commands.command()  # Manually adds an entry to the database [ONLY TO BE USED IN TESTING]
    async def dbadd(self, ctx, serverID, serverName):
        mndb = mongoclient['the-random-bot']  # Connects to the database
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)  # Error embed
        if str(ctx.message.author.id) in config['developers']:  # Checks if someone is in the developers dictionary
            mndb.servers.insert_one(
                {"serverID": serverID,
                 "serverName": serverName
                 }
            )  # Adds to the new entry to the database
            await ctx.channel.send("Added to the database.")
        else:
            await ctx.channel.send(embed=notDevEmbed)

    @commands.command()  # Manually removes an entry from the database [ONLY TO BE USED IN TESTING]
    async def dbrm(self, ctx, serverID):
        mndb = mongoclient['the-random-bot']  # Connects to the database
        servercol = mndb["servers"]  # The column to be used
        deleteQuery = {"serverID": serverID}  # The query to be deleted
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)  # Error Embed
        if str(ctx.message.author.id) in config['developers']:  # Checks if someone is in the developer dictionary
            servercol.delete_one(deleteQuery)  # Deletes the entry
            await ctx.channel.send("Removed from the database.")
        else:
            await ctx.channel.send(embed=notDevEmbed)


def setup(bot):
    bot.add_cog(DB(bot))
