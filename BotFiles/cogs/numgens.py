from discord.ext import commands
import discord
from random import randint, choice
import decimal


class NumGens(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()  # Fibonacci nth number
    async def fibonacci(self, ctx, num: int):
        fibArray = [0, 1]

        def fibonacci(n):
            if n < 0:
                raise ValueError("Cannot use below 0")
            elif n <= len(fibArray):
                return fibArray[n - 1]
            else:
                temp_fib = fibonacci(n - 1) + fibonacci(n - 2)
                fibArray.append(temp_fib)
                return temp_fib

        try:
            fibNum = fibonacci(num)
            fibonacciEmbed = discord.Embed(title=f"The {num}th Number of the Fibonacci Sequence is:",
                                           description=fibNum,
                                           color=0xB87DDF)
            await ctx.channel.send(embed=fibonacciEmbed)
        except:
            fiboError = discord.Embed(title="Invalid Input",
                                      description="Did you use a negative, invalid, or an input that's too high?",
                                      color=0xC73333
                                      )
            await ctx.channel.send(embed=fiboError)

    @commands.command()  # Dice roll
    async def roll(self, ctx, sides: int = 6):
        if len(str(sides)) > 256:
            tooBigEmbed = discord.Embed(title="Error: Too Long",
                                        description="You entered a number that filled up the embed's title, please use a shorter one.",
                                        color=0xC73333
                                        )
            await ctx.channel.send(embed=tooBigEmbed)

        elif sides < 0:
            negEmbed = discord.Embed(title="...",
                                     description=f"*Somehow rolls a {sides} sided die*\n\nGood job, you broke the laws of physics. As we speak a black hole has formed and will envelop the universe in less than 24 hours. Hope you're happy jerk.",
                                     color=0xC73333
                                     )
            await ctx.channel.send(embed=negEmbed)

        elif sides == 0:
            zeroEmbed = discord.Embed(title="Yeah, sure",
                                      description="Good luck with that.",
                                      color=0xC73333
                                      )
            await ctx.channel.send(embed=zeroEmbed)
        else:
            roll = randint(1, sides)
            dieEmbed = discord.Embed(title=f"You Rolled a {sides} sided die",
                                     description=f"You rolled a __**{roll}**__!",
                                     color=0xB87DDF)
            await ctx.channel.send(embed=dieEmbed)

    @commands.command()  # Calculate Pi to the Nth digit using Chudnovsky's Algorithm
    async def pi(self, ctx, num: int):
        def compute_pi(n):
            decimal.getcontext().prec = n + 1
            C = 426880 * decimal.Decimal(10005).sqrt()
            K = 6.
            M = 1.
            X = 1
            L = 13591409
            S = L
            for i in range(1, n):
                M = M * (K ** 3 - 16 * K) / ((i + 1) ** 3)
                L += 545140134
                X *= -262537412640768000
                S += decimal.Decimal(M * L) / X
            pi = C / S
            return pi

        try:
            piEmbed = discord.Embed(title=f"Pi to the {num}th Digit is:",
                                    description=str(compute_pi(num)),
                                    color=0xB87DDF
                                    )
            piEmbed.set_footer(text="Disclaimer: Value might be off by a slight amount due to rounding")
            await ctx.channel.send(embed=piEmbed)
        except:
            piError = discord.Embed(title="Error: Invalid Input",
                                    description="Invalid input, did you use a negative number?",
                                    color=0xC73333
                                    )

    @commands.command()  # Random Number Generator - Generates a random number
    async def number(self, ctx, low: int = 0, high: int = 100, type=None):
        if low > high: low, high = high, low
        # The error it gives if there is no high or low
        numError = "There was an error! Did you make sure to give numbers and not words?"
        errorEmbed = discord.Embed(title="Error:",
                                   description=numError,
                                   color=0xC73333)
        ran = randint(low, high)
        numEmbed = discord.Embed(title=f"Your Random Number Between {low} and {high} is", description=ran,
                                 color=0xB87DDF)
        if type is None:
            try:
                await ctx.channel.send(embed=numEmbed)
            except discord.Forbidden:
                await ctx.channel.send(embed=errorEmbed)
        elif type.lower() == "prime":
            def getRandomPrimeInteger(bounds):

                for i in range(bounds.__len__() - 1):
                    if bounds[i + 1] > bounds[i]:
                        x = bounds[i] + np.random.randint(bounds[i + 1] - bounds[i])
                        if isPrime(x):
                            return x

                    else:
                        if isPrime(bounds[i]):
                            return bounds[i]

                    if isPrime(bounds[i + 1]):
                        return bounds[i + 1]

                newBounds = [0 for i in range(2 * bounds.__len__() - 1)]
                newBounds[0] = bounds[0]
                for i in range(1, bounds.__len__()):
                    newBounds[2 * i - 1] = int((bounds[i - 1] + bounds[i]) / 2)
                    newBounds[2 * i] = bounds[i]

                return getRandomPrimeInteger(newBounds)

            def isPrime(x):
                count = 0
                for i in range(int(x / 2)):
                    if x % (i + 1) == 0:
                        count = count + 1
                return count == 1

            bounds = [low, high]
            # Gets 1 prime number from the range
            for i in range(1):
                boundsError = discord.Embed(title="Bounds Error",
                                            description="Sorry, it appears I was not made to handle your cryptographical needs. Please try a high and low below 2^20",
                                            color=0xC73333
                                            )
                if low >= 1048576 or high >= 1048576:
                    await ctx.channel.send(embed=boundsError)
                    break
                x = getRandomPrimeInteger(bounds)
                primeEmbed = discord.Embed(title="Random Prime Number",
                                           description=x,
                                           color=0xB87DDF
                                           )
                await ctx.channel.send(embed=primeEmbed)

        elif type.lower() == "odd":
            odd = ran % 2
            if odd == 1:
                oddEmbed = discord.Embed(title="Random Odd Number", description=ran, color=0xB87DDF)
                await ctx.channel.send(embed=oddEmbed)

            else:
                ran += 1
                oddEmbed = discord.Embed(title="Random Odd Number", description=ran, color=0xB87DDF)
                await ctx.channel.send(embed=oddEmbed)
        elif type.lower() == "even":
            even = ran % 2
            if even == 0:
                evenEmbed = discord.Embed(title="Random Even Number", description=ran, color=0xB87DDF)
                await ctx.channel.send(embed=evenEmbed)
            else:
                ran += 1
                evenEmbed = discord.Embed(title="Random Even Number", description=ran, color=0xB87DDF)
                await ctx.channel.send(embed=evenEmbed)


def setup(bot):
    bot.add_cog(NumGens(bot))
