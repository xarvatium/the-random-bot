from discord.ext import commands
import discord
import urbanpython
import json

with open('./config.json') as configFile:
    config = json.load(configFile)


class NSFWOnly(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def urban(self, ctx, *, query):
        urban = urbanpython.Urban(config['urbanKey'])
        result = urban.search(query)
        if ctx.channel.is_nsfw():
            urbanEmbed = discord.Embed(
                title=f'Result for "{query}"',
                description=f"[Permalink]({result.permalink}) {result.definition}",
                color=0xB87DDF
            )
            urbanEmbed.set_author(name=f"Written By: {result.author}")
            urbanEmbed.set_footer(
                text=f"Thumbs Up: {result.thumbs_up} | Thumbs Down: {result.thumbs_down} | ID: {result.defid} | Written On: {result.written_on}"
            )
            await ctx.channel.send(embed=urbanEmbed)
        else:
            errorEmbed = discord.Embed(title="Error: NSFW Command Only",
                                       description="Due to certain content on Urban Dictionary, this command is restricted to NSFW channels only.",
                                       color=0xC73333
                                       )
            await ctx.channel.send(embed=errorEmbed)


def setup(bot):
    bot.add_cog(NSFWOnly(bot))
