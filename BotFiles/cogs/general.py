from discord.ext import commands
import discord
import asyncio
import json


# Having helpText assigned here will cause helptext to only get read once, reducing disk access
helpText = {}
with open('./helpText') as helpFile:
    helpString = helpFile.read()
helpText['general'] = helpString.split("<general>\n")[1].split("\n</general>")[0]
helpText['random'] = helpString.split("<random>\n")[1].split("\n</random>")[0]
helpText['numgen'] = helpString.split("<numgen>\n")[1].split("\n</numgen>")[0]
helpText['misc'] = helpString.split("<misc>\n")[1].split("\n</misc>")[0]
helpText['support'] = helpString.split("<support>\n")[1].split("\n</support>")[0]

with open('./config.json') as configFile:
    config = json.load(configFile)


class General(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()  # The help command
    async def help(self, ctx):
        bot = self.bot
        titles = ["General Commands", "Text Generation", "Number Generation", "Misc.", "Support"]
        contents = [f"{helpText['general']}", f"{helpText['random']}", f"{helpText['numgen']}", f"{helpText['misc']}",
                    f"{helpText['support']}"]
        pages = 5
        cur_page = 1
        helpEmbed = discord.Embed(title=f"Page {cur_page}/{pages}: {titles[cur_page - 1]}",
                                  description=f"{contents[cur_page - 1]}",
                                  color=0xB87DDF)
        helpEmbed.set_footer(text="Creator: Xarvatium#6561",
                             icon_url="https://xarvatium.dev/icon.png?size=128"
                             )
        helpEmbed.set_author(name="The Random Bot", url="https://github.com/xarvatium/the-random-project",
                             icon_url="https://cdn.discordapp.com/avatars/755986454907191319/39f37a55eff9e855b449076b65837b91.png?size=128")
        message = await ctx.send(embed=helpEmbed)
        # getting the message object for editing and reacting

        await message.add_reaction("◀️")
        await message.add_reaction("▶️")
        await message.add_reaction("❌")

        def check(reaction, user):
            return user == ctx.author and str(reaction.emoji) in ["◀️", "▶️", "❌"]
            # This makes sure nobody except the command sender can interact with the "menu"

        while True:
            try:
                reaction, user = await bot.wait_for("reaction_add", timeout=30, check=check)
                # waiting for a reaction to be added - times out after x seconds, 60 in this
                # example

                if str(reaction.emoji) == "▶️" and cur_page != pages:
                    cur_page += 1
                    helpEmbed = discord.Embed(title=f"Page {cur_page}/{pages}: {titles[cur_page - 1]}",
                                              description=f"{contents[cur_page - 1]}",
                                              color=0xB87DDF)
                    helpEmbed.set_footer(text="Creator: Xarvatium#6561",
                                         icon_url="https://xarvatium.dev/icon.png?size=128"
                                         )
                    helpEmbed.set_author(name="The Random Bot", url="https://github.com/xarvatium/the-random-project",
                                         icon_url="https://cdn.discordapp.com/avatars/755986454907191319/39f37a55eff9e855b449076b65837b91.png?size=128")
                    await message.edit(embed=helpEmbed)
                    await message.remove_reaction(reaction, user)

                elif str(reaction.emoji) == "◀️" and cur_page > 1:
                    cur_page -= 1
                    helpEmbed = discord.Embed(title=f"Page {cur_page}/{pages}: {titles[cur_page - 1]}",
                                              description=f"{contents[cur_page - 1]}",
                                              color=0xB87DDF)
                    helpEmbed.set_footer(text="Creator: Xarvatium#6561",
                                         icon_url="https://xarvatium.dev/icon.png?size=128"
                                         )
                    helpEmbed.set_author(name="The Random Bot", url="https://github.com/xarvatium/the-random-project",
                                         icon_url="https://cdn.discordapp.com/avatars/755986454907191319/39f37a55eff9e855b449076b65837b91.png?size=128")
                    await message.edit(embed=helpEmbed)
                    await message.remove_reaction(reaction, user)

                elif str(reaction.emoji) == "❌":
                    await message.delete()

                else:
                    await message.remove_reaction(reaction, user)
                    # removes reactions if the user tries to go forward on the last page or
                    # backwards on the first page
            except asyncio.TimeoutError:
                await message.delete()
                break
                # ending the loop if user doesn't react after x seconds

    @commands.command()  # Ping command
    async def ping(self, ctx):
        bot = self.bot
        ping = bot.latency * 1000  # Multiplies the latency by 1000 to get milliseconds
        pingEmbed = discord.Embed(title="Pong! :ping_pong:", description=f"My latency is: **{int(ping)}ms**",
                                  color=0xB87DDF)
        await ctx.channel.send(embed=pingEmbed)  # Sends the embed

    @commands.command()  # :)
    async def monke(self, ctx):
        await ctx.channel.send(
            "https://tenor.com/view/obese-monkey-fat-monkey-summer-belly-eating-lettuce-summer-look-gif-13014350"
        )

    @commands.command()  # Repeat command
    async def repeat(self, ctx, *, userinput=None):
        sentByMention = str(ctx.author.mention)
        for s in config['bannedWords']:
            # Reads from the bannedWords list and removes anything on the list from the text
            userinput = userinput.replace(s, '')
        # Defining the embed to be used and it's field
        repeatEmbed = discord.Embed(description=userinput, color=0xB87DDF)
        repeatEmbed.add_field(name="Sent by:", value=sentByMention)
        if userinput:  # The If statement that checks for "im" or "@everyone"/"@here"
            if userinput.lower().startswith("im"):
                await ctx.send("yea we know")
            elif userinput.lower().startswith('i '):
                await ctx.send("yea we know")
            elif userinput.lower().startswith("i'm"):
                await ctx.send("yea we know")
            elif userinput.lower() == "@everyone":
                await ctx.send("no :)")
            elif userinput.lower() == "@here":
                await ctx.send("no :)")
            else:  # The else statement that sends the full message if it passes the previous el/if statements
                await ctx.send(embed=repeatEmbed)
        else:  # The else statement that checks if there is no user input argument
            # The error embed that sends if there is no argument
            noUserIn = discord.Embed(title="Error",
                                     description="Sorry! It appears you didn't include something for me to repeat!",
                                     color=0xC73333
                                     )  # Makes the embed for if there is no user input
            await ctx.channel.send(embed=noUserIn)

    @commands.command()  # 8Ball module (used to be a separate file but caused issues)
    async def ask(self, ctx, *, content):
        import random
        # Defines the array of responses
        responses = ["It is certain", "Without a doubt", "You may rely on it", "Yes, definitely", "It is decidedly so",
                     "As I see it, yes", "Most likely", "Yes", "Outlook good", "Signs point to yes",
                     "Reply hazy try again",
                     "Better not tell you now", "Ask again later", "Cannot predict now", "Concentrate and ask again",
                     "Don't count on it", "Outlook not so good", "My sources say no", "Very doubtful", "My reply is no"]
        answer = random.choice(responses)  # Gets a random response
        await ctx.channel.send(answer)  # Sends the answer


def setup(bot):
    bot.add_cog(General(bot))
