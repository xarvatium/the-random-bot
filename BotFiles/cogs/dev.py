from discord.ext import commands
import discord
import json

with open('./config.json') as configFile:
    config = json.load(configFile)


def generate_config_reload():
    global config
    with open('config.json') as configFile:
        config = json.load(configFile)


class Dev(commands.Cog, commands.CheckFailure):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()  # Lists servers the bot is in
    async def servers(self, ctx):  # Developer command
        bot = self.bot
        servers = list(bot.guilds)
        serversEmbedTitle = f"Connected on {str(len(servers))} servers"
        serversEmbedDesc = "- " + '\n- '.join(guild.name for guild in servers)
        serversEmbed = discord.Embed(title=serversEmbedTitle, description=serversEmbedDesc, color=0xB87DDF)
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)
        id = ctx.message.author.id
        if str(ctx.message.author.id) in config['developers']:
            await ctx.send(embed=serversEmbed)

        else:
            await ctx.channel.send(embed=notDevEmbed)

    @commands.command()  # Changes the status
    async def status(self, ctx, *, content):  # Developer command that changes the bot's status
        bot = self.bot
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)
        if str(ctx.message.author.id) in config['developers']:
            await bot.change_presence(
                activity=discord.Game(
                    name=content
                )
            )
        else:
            await ctx.channel.send(embed=notDevEmbed)

    @commands.command()  # Makes a developer
    async def mkdev(self, ctx, userid=None, *, devName=None):
        for i in ["<", ">", "@", "!"]:  # Makes @Person work too
            userid = userid.replace(i, '')

        # Embed giving an error if the user is not in the developer array in the config.json file
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)
        if str(ctx.message.author.id) in config['developers']:  # Checking if a user is a developer
            if not userid or not devName:  # Checking if a user gave proper arguments
                await ctx.channel.send("Usage: ;mkdev <userid> <name>")
                return
            if userid not in config['developers']:  # Checking if a user's id is not in the developer section
                config['developers'][userid] = devName
                await ctx.channel.send("Successfully added <@" + userid + "> as a developer")  # Sends a success message
                with open('config.json', 'w+') as configFile:  # Opens the config.json file
                    json.dump(config, configFile, indent=4)
                    generate_config_reload()
            else:  # Checks if a user is already a developer
                await ctx.channel.send("Error: " + userid + " is already a developer")
        else:  # What sends the notDevEmbed
            await ctx.channel.send(embed=notDevEmbed)

    @commands.command()  # Removes a developer
    async def rmdev(self, ctx, userid=None):
        for i in ["<", ">", "@", "!"]:  # Makes @Person work too
            userid = userid.replace(i, '')
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)  # Error Embed
        if str(ctx.message.author.id) in config['developers']:  # Checks if someone is in the developer dictionary
            if not userid:  # Error to send if no ID is defined
                await ctx.channel.send("Usage: ;rmdev <userid>")
                return
            if userid in config['developers']:  # Continues if an ID is defined
                config['developers'].pop(userid)  # Adds ID to the dictionary
                await ctx.channel.send("Successfully removed <@" + userid + "> as a developer")
                with open('config.json', 'w+') as configFile:  # Writes to config.json
                    json.dump(config, configFile, indent=4)
                    generate_config_reload()
            else:
                await ctx.channel.send("Error: " + userid + " is not a developer")
        else:
            await ctx.channel.send(embed=notDevEmbed)

    @commands.command()  # Lists the developers
    async def lsdev(self, ctx):
        notDevEmbed = discord.Embed(title="Error",
                                    description="Sorry! It appears you don't have permission to use this command.",
                                    color=0xC73333)  # Error Embed
        if str(ctx.message.author.id) in config['developers']:  # Checks if user is in the developer dictionary
            devsEmbed = discord.Embed(title="List of current developers: ")
            for i in config['developers']:  # Iterates through all developers in the dictionary
                devsEmbed.add_field(value=i, name=config['developers'][i], inline=False)
            await ctx.channel.send(embed=devsEmbed)
        else:
            await ctx.channel.send(embed=notDevEmbed)

    @commands.command()
    async def reloadcog(self, ctx, cog):
        bot = self.bot
        await ctx.channel.send(f"Attempting to reload cog {cog}")
        bot.reload_extension(f"cogs.{cog}")




def setup(bot):
    bot.add_cog(Dev(bot))
